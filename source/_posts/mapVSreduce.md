# Difference between map and reduce method
 
We look at this point to understand difference between map and reduce method:

* Definitions
* Return value
* Ability to chain other methods
* Mutability
* Final Thoughts

## Definitions:
  **Array.prototype.map()**

  The map method takes every array element to creates a new array from callback function provided as an arguments. For empty elements it does not execute the function. The original array will not change.

  It return a new array with the same length as the original array.

  **Syntax**

  The map method is represented by the following syntax:
  ```
  array.map(callback(currentValue, index, array), thisValue) 

  (or)

  array.map((currentValue) => {
   // write code here
  }) 
  ```

  **Array.prototype.reduce()**

  The reduce method runs the callback function on each elements of the calling array which results in a single output value. The reduce method accepts two parameters are reducer or callback function  and  an optional initialValue. For empty elements it does not execute the function. The original array will not change.

  It returns a single value array. The last call of the callback function gives its single value output.

  **Syntax**

  The reduce method is represented by the following syntax:
  ```
  array.reduce(callback(accumulator, currentValue, index, array), initialValue)

  (or)

  array.reduce((accumulator, currentValue) => {
      // write code here 
  },initialValue)
  ```

## Return value:

The first difference between map() and reduce() is the returning value. The reduce() method returns single value and map() returns a new array with the changed elements.

```
const number = [1, 2, 3, 4, 5];
const squareNumber = number.map((element) => {
  return element * element;
});

console.log(squareNumber); // [ 1, 4, 9, 16, 25 ]

const multiplyNumber = number.reduce((accumulator, element) => {
  return accumulator * element;
});

console.log(multiplyNumber); // 120
```

The above example explained the difference between the output of the map and reduce method by just performing a simple operation through it. The map method gives the same length of an array that we take as input only changed in elements value. The reduce method gives output in a single value.


## Ability to chain other methods:

The second difference between these array methods is common we can do chaining with both of them. This means that you can attach sort(), filter() and so on method on an array.
```
const number = [1, 2, 3, 4, 5];

const chainMethod = number
  .filter((element) => {
    return element < 4;
  })
  .map((element) => {
    return element * element;
  })
  .reduce((total, squareElement) => {
    return total + squareElement;
  });

console.log(chainMethod); // 14

```
In above code shows how we can chain one method or more to an array to perform certain operations.

## Mutability:

The type of variable can be changed is called as mutable. In JavaScript, only array and object are mutable. It means when object or array created after that we can modified it.

And Immutables are those who cant modified its original value once it created.

Map and reduce method is immutables because once array is created after that we can perform operation but can not change original array. In map method return new array and reduce method return a single value. But overall they does not modifies the original array.

See below example:
```

const number = [1, 2, 3, 4, 5];

 number.map((element) => {
  return element * 2;
}); // return result = [ 2, 4, 6, 8, 10 ]

number.reduce((total, element) => {
  return total + element;
}); // return result = 15

console.log(number); // [ 1, 2, 3, 4, 5 ]

```

In above code shows the map and reduce methods are immutable because they do not change the original array.

## Final Thoughts:

Both map and reduce methods have as input the array and perform the function you define. They are in some way: map returns a new array for a specific operation of an array of multiple elements, while reduce method will always return an accumulator you changed eventually.
