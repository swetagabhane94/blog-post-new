
# Error handling with promises

Error handling is a way how we find bugs and solved them as quickly as possible. Handing errors means a properly developed codebase and reduced timing to find bugs and errors.

Error handling is important because we have to fix bugs less painful manner. It helps us to write code cleaner way. It gives an idea of when and how our code breaks. If we don't follow proper error-handling practices, they can make code debugging difficult.

Promises are used to handle asynchronous operations in JavaScript. It is an object representing the eventual completion or failure of an asynchronous operation. The promise chains are a better way of error handling.

When the promise rejects, the controls go to the near-rejection handler. Mostly used method of error handling is `.catch()`.


Let's look at some example:
```
 fetch("https://jsonplaceholder.ksers")
  .then((data) => {
    return data.json();
  })
  .catch((err) => {
    console.error(err);
  }); // TypeError: fetch failed


  fetch("/")
  .then((data) => {
    return data.json();
  })
  .catch((err) => {
    console.error(err);
  }); // TypeError: Failed to parse URL from

```

The above code explains the how catch handle the error when wrong URL or invalid fetching perform.

In promise error handler has an invisible try..catch around it. If executor automatically found the error and deal as a rejection.

Let's look at some example:
```
new Promise((resolve, reject) => {
  throw new Error("Showing Error!");
})
.catch((err) => {
  console.error(err);
}); // Error: Showing Error!


new Promise((resolve, reject) => {
  reject(new Error("Error Show!"));
})
.catch((err) => {
  console.error(err);
}); // Error: Error Show!
```

In above code, both works same manner showing error because executor automatically catches the error.
It trears as a rejection showing error in terminal.

## why error handling is important with promises

Error handling is important because we want to fix the bugs in a less painful manner. It gives a clear structure of how and when our code is broken and code writing is better when we use error handling at a certain position. And easy to debug if we practice error handling.

In promises, we use `.catch()` to handle rejection it will be a `reject()` call or error thrown. we should place catch where we want to handle errors or how to handle the error. If we did not use .catch it is all ok but we doesn't run the whole program it will display only an error and recovering from an error is difficult.


